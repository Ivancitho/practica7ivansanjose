package programa;

import java.io.IOException;
import java.util.Scanner;

import clases.FicherosAccesoAleatorio;
import clases.FicherosSecuenciales;

public class Programa {

	public static void main(String[] args) throws IOException {
	
		int opcion;
		
		Scanner input = new Scanner(System.in);
		
		System.out.println("Introduce el nombre del archivo");
		String nombre = input.nextLine();
		
		FicherosSecuenciales miFichero = new FicherosSecuenciales(nombre);
		FicherosAccesoAleatorio miFichero2 = new FicherosAccesoAleatorio(nombre);
		
		
		do {
			
			opcion = 0;
			
			System.out.println("___________________________________MENU___________________________________");
			System.out.println("1- Crear archivo de personajes ('salir' para acabar)");
			System.out.println("2- Visualiza los personajes");
			System.out.println("3- Busca el personaje que quieras en el archivo");
			System.out.println("4- Escribir archivo (RAF)");
			System.out.println("5- Visualizar el archivo (RAF)");
			System.out.println("6- Modificar el archivo (RAF)");
			System.out.println("7- Accion extra n� 1: Invertir el archivo");
			System.out.println("8- Accion extra RAF n� 1: Cambiar letras");
			System.out.println("9- Accion extra n� 2.1: Conversion del texto del archivo a mayusculas");
			System.out.println("10- Accion extra n� 2.2: Conversion del texto del archivo a minusculas");
			System.out.println("11- Accion extra n�3: Cuenta la cantidad de lineas del archivo");
			System.out.println("12- Salir del Menu");
			System.out.println("__________________________________________________________________________");
			opcion = input.nextInt();
			
			switch(opcion) {
			case 1:
				miFichero.creacionPersonajes();
				break;
			case 2:
				miFichero.visualizar();
				break;
			case 3:
				miFichero.buscarPersonajes();
				break;
			case 4:
				miFichero2.escribir();
				break;
			case 5:
				miFichero2.viualizar();
				break;
			case 6:
				miFichero2.modificarArchivo();
			case 7:
				miFichero.archivoInvertido();
				break;
			case 8:
				miFichero2.cambiarLetras();
				break;
			case 9:
				miFichero.conversiones(1);
				break;
			case 10:
				miFichero.conversiones(2);
				break;
			case 11:
				miFichero.cantidadLineas();
				break;
			case 12:
				System.exit(0);
			}
		}while(opcion < 13);
	}

}
