package clases;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Scanner;
import java.util.Vector;

import javax.sound.midi.Synthesizer;
import javax.swing.plaf.synth.SynthSpinnerUI;

public class FicherosSecuenciales {

	private String archivo;

	public FicherosSecuenciales(String archivo) {
		this.archivo = archivo;
	}
	
	public void creacionPersonajes() throws IOException {
		System.out.println("Introduce el nombre de los personajes");
		BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
		String lineas;
		
		PrintWriter letras = new PrintWriter(new BufferedWriter(new FileWriter(this.archivo)));
		System.out.println("Creacion de los personajes, introduce la cantidad de personajes que quieras (1 por linea), ('salir' para acabar)");
		
		lineas = in.readLine();
		
		while(!lineas.equalsIgnoreCase("salir")) {
			letras.println(lineas);
			lineas = in.readLine();
		}
		
		System.out.println("El archivo con los personajes se ha creado correctamente");
		
		letras.close();
	}
	
	public void visualizar() throws IOException {
		String lineas;
		System.out.println("Visualizar archivo" + this.archivo);
		
		BufferedReader letras = new BufferedReader(new FileReader(this.archivo));
		
		lineas = letras.readLine();
		
		while(lineas != null) {
			System.out.println(lineas);
			lineas = letras.readLine();
		}
		
		letras.close();
	}
	
	public void buscarPersonajes() {// rehacer
		String busco;
		String linea;
		Scanner input = new Scanner(System.in);
		System.out.println("Busco la palabra que quiera");
		System.out.println("Introduce la palabra que quieras buscar");
		
		busco = input.nextLine();
		
		Vector<String> vector = new Vector<String>();
		
		try {
			BufferedReader letras = new BufferedReader(new FileReader(this.archivo));
			
			linea = letras.readLine();
			
			while(linea != null) {
				String letra;
				String palabras = "";
				for(int i = 0; i < linea.length(); i++) {
					letra = linea.substring(i, i + 1);
					if (letra.equalsIgnoreCase(" ") == false) {
						palabras = palabras + letra;
						System.out.println(palabras);
					}
				}
				vector.add(palabras);
				palabras = "";
				linea = letras.readLine();
			}
		
			int palabra = 0;
			for(int i = 0; i < vector.size(); i++) {
				String valor = vector.elementAt(i).toString();
				if(valor.equalsIgnoreCase(busco) == true) {
					palabra++;
					System.out.println("El personaje encontrado es: "+ busco);
				}
			}
			if (palabra == 0) {
				System.out.println("El personaje que buscas no esta en el archivo");
			}
			else {
				System.out.println("El personaje que busca esta en el archivo " + palabra + " veces");
			}
			
			letras.close();
			
		} catch (FileNotFoundException e) {
			System.out.println("No se ha encontrado el fichero");
			System.exit(0);
		} catch (IOException e) {
			System.out.println("Error de entrada o salida");
			System.exit(0);
		}
	}
	
	//Accion extra Numero 1
	public void archivoInvertido() throws IOException {
		BufferedReader principio = new BufferedReader(new FileReader(archivo));
		ArrayList<String> vector = new ArrayList<String>();
		
		String linea = "";
		linea = principio.readLine();
		while(linea != null) {
			vector.add(linea);
			linea = principio.readLine();
		}
		
		System.out.println("El orden de los archivos se ha invertido, pulse la opcion 2 del menu para visualizarlo");
		
		principio.close();
		
		PrintWriter fin = new PrintWriter(new FileWriter(archivo, false));
		
		for(int i = vector.size() - 1; i >= 0; i--) {
			fin.println(vector.get(i));
		}
		fin.close();
	}
	
	//Accion extra n�2
	public void conversiones(int opciones) {
		String lineas;
		System.out.println("Convertir a Minusculas o a Mayusuculas");
		if (opciones == 1) {
			System.out.println("Has elegido Mayusculas, contenido del archivo escrito en mayusculas, eliga la opcion 2 para visualizarlo");
		}
		else {
			System.out.println("Has elegido Minuscula, contenido del archivo escrito en minusculas, eliga la opcion 2 para visualizarlo");
		}
		
		ArrayList<String> vector = new ArrayList<String>();
		
		try {
			BufferedReader file = new BufferedReader(new FileReader(this.archivo));
			lineas = file.readLine();
			while (lineas != null) {
				vector.add(lineas);
				lineas = file.readLine();
			}
			
			file.close();
			
			PrintWriter archivo = new PrintWriter(new FileWriter(this.archivo));
			for(int i = 0; i < vector.size(); i++) {
				if(opciones == 1) {
					archivo.println(vector.get(i).toUpperCase());
				}
				else {
					archivo.println(vector.get(i).toLowerCase());
				}
			}
			
			archivo.close();
		} catch (FileNotFoundException e) {
			System.out.println("No se ha podido encontrar el archivo");
			System.exit(0);
		} catch (IOException e) {
			System.out.println("Error");
			System.exit(0);
		}
	}
	
	//Accion extra n�3
	public void cantidadLineas() {
		System.out.println("Contador de lineas");
		String lineas;
		int contador = 0;
		
		try {
			BufferedReader cantidad = new BufferedReader(new FileReader(this.archivo));
			
			lineas = cantidad.readLine();
			
			while(lineas != null) {
				contador++;
				lineas = cantidad.readLine();
			}
			
			cantidad.close();
		} catch (FileNotFoundException e) {
			System.out.println("No se ha encontrado el archivo");
			System.exit(0);
		} catch (IOException e) {
			System.out.println("Error");
			System.exit(0);
		}
		System.out.println("La cantidad de lineas del archivo es: " + contador);
	}
	
}
