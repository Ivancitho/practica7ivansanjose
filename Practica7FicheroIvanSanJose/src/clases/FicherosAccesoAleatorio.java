package clases;

import java.io.BufferedReader;
import java.io.EOFException;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.RandomAccessFile;

public class FicherosAccesoAleatorio {
	
	String archivo;

	public FicherosAccesoAleatorio(String archivo) {
		this.archivo = archivo;
	}

	public void escribir() {
		try {
			BufferedReader input = new BufferedReader(new InputStreamReader(System.in));
			RandomAccessFile file = new RandomAccessFile(archivo,"rw");
			file.seek(file.length());
			String respuesta = "";
			do {
				System.out.println("Nombre ");
				String nombre = input.readLine();
				nombre = cambiarArchivo(nombre, 20);
				file.writeUTF(nombre);
				System.out.println("�Quieres continuar? (si/no)");
				respuesta = input.readLine();
			}while (respuesta.equalsIgnoreCase("si"));
			file.close();
		} catch (FileNotFoundException e) {
			System.out.println("Error archivo no encontrado");
		} catch (IOException e) {
			System.out.println("Error");
		}
	}
	
	public void viualizar() {
		try {
			RandomAccessFile file = new RandomAccessFile(archivo,"rw");
			String nombre = "";
			boolean terminaFichero = false;
			do {
				try {
				nombre = file.readUTF();
				System.out.println(nombre);
				}catch(EOFException e) {
					System.out.println("Final del fichero");
					terminaFichero = true;
					file.close();
				}
			}while(!terminaFichero);
		}catch (IOException e) {
			System.out.println("Error");
		}
	}
	
	private String cambiarArchivo(String nombre, int longitud) {
		if(nombre.length() > longitud) {
			return nombre.substring(0, longitud);
		}
		else {
			for(int i = nombre.length(); i < longitud; i++) {
				nombre = nombre + "";
			}
		}
		return nombre;
	}
	
	public void modificarArchivo() {
		try {
		BufferedReader input = new BufferedReader(new InputStreamReader(System.in));
		String oldFile = archivo;
		String newFile = archivo;
		System.out.println("Archivo a modificar");
		oldFile = input.readLine();
		System.out.println("Rellena el archivo");
		newFile = input.readLine();
		
		RandomAccessFile file = new RandomAccessFile(archivo, "rw");
		
		String nombre;
		boolean terminaFichero = false;
		boolean valor = false;
		do {
			try {
				nombre = file.readUTF();
				if(nombre.trim().equalsIgnoreCase(oldFile)) {
					file.seek(file.getFilePointer() - 22);
					newFile = cambiarArchivo(newFile, 20);
					file.writeUTF(newFile);
					valor = true;
				}
			}catch (EOFException e) {
				file.close();
				terminaFichero = true;
			}
		}while(!terminaFichero);
		if(valor == false) {
			System.out.println("El valor no se encuentra en el archivo");
		}
		else {
			System.out.println("El valor del archivo se ha cambiado");
		}
		} catch (IOException e) {
			System.out.println("Error");
		}
	}
	
	//Accion extra n� 1
	public void cambiarLetras() {
		try {
			RandomAccessFile file = new RandomAccessFile(archivo, "rw");
			char letra;
			boolean finalFichero = false;
			
			do {
				try {
					letra = (char) file.readByte();
					if(letra == 'a') {
						file.seek(file.getFilePointer() - 1);
						file.writeByte('A');
					}
				} catch (IOException e) {
					System.out.println("Se ha completado el cambio, visualize el archivo para ver los cambios");
					finalFichero = true;
					file.close();
				}
			}while(finalFichero == false);
		} catch (IOException e) {
			System.out.println("Error");
		}
	}
	
}
